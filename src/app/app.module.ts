import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APP_ROUTING } from './app.route';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DepartamentosComponent } from './components/departamentos/departamentos.component';
import { DepartamentoService } from './services/departamento.service';
import { RolesComponent } from './components/roles/roles.component';
import { RolService } from './services/rol.service';
import { FooterComponent } from './components/footer/footer.component';
import { PermisosComponent } from './components/permisos/permisos.component';
import { PermisoService } from './services/permiso.service';
import { NivelAccesoComponent } from './components/nivel-acceso/nivel-acceso.component';
import { NivelAccesoService } from './services/nivel-acceso.service';
import { PermisoNivelAccesoComponent } from './components/permiso-nivel-acceso/permiso-nivel-acceso.component';
import { PermisoNivelAccesoService } from './services/permiso-nivel-acceso.service';
import { AsistenciasComponent } from './components/asistencias/asistencias.component';
import { AsistenciaService } from './services/asistencia.service';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { UsuarioService } from './services/usuario.service';
import { PersonaService } from './services/persona.service';
import { LoginComponent } from './components/login/login.component';
import { PersonasComponent } from './components/personas/personas.component';
import { AsignacionPersonasComponent } from './components/asignacion-personas/asignacion-personas.component';
import { CreatePersonaComponent } from './components/personas/create-persona/create-persona.component';
import { DetailsPersonaComponent } from './components/personas/details-persona/details-persona.component';
import { UpdatePersonaComponent } from './components/personas/update-persona/update-persona.component';
import { AsignacionPersonaService } from './services/asignacion-persona.service';
import { UbicacionService } from './services/ubicacion.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DashboardComponent,
    DepartamentosComponent,
    RolesComponent,
    FooterComponent,
    PermisosComponent,
    NivelAccesoComponent,
    PermisoNivelAccesoComponent,
    AsistenciasComponent,
    UsuariosComponent,
    LoginComponent,
    PersonasComponent,
    AsignacionPersonasComponent,
    CreatePersonaComponent,
    DetailsPersonaComponent,
    UpdatePersonaComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    HttpModule,
    FormsModule,
    APP_ROUTING
  ],
  providers: [
    DepartamentoService,
    RolService,
    PermisoService,
    NivelAccesoService,
    PermisoNivelAccesoService,
    AsistenciaService,
    PersonaService,
    AsignacionPersonaService,
    UsuarioService,
    UbicacionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
