import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Observable';

@Injectable()
export class DepartamentoService {

  private ruta = 'http://192.168.44.109:4000/api/v1/departamentos';
  private rutaCUD = 'http://192.168.44.109:4000/api/v1/departamento/';

  constructor(private http: Http) { }

  getOpciones(): RequestOptions {
    const HEADERS = new Headers({ name: 'application/json' });
    const REQOPTIONS = new RequestOptions({ headers: HEADERS });

    return REQOPTIONS;
  }

  getDepartamentos() {
    return this.http.get(this.ruta, this.getOpciones());
  }

  getPersonas(idDepartamento) {
    return this.http.get(this.rutaCUD + idDepartamento + '/personas', this.getOpciones());
  }

  createDepartamento(nombre: string) {
    const BODY = { nombreDepartamento: nombre };
    return this.http.post(this.rutaCUD, BODY);
  }

  updateDepartamento(id: number, nombre: string) {
    const BODY = { idDepartamento: id, nombreDepartamento: nombre };
    return this.http.put(this.rutaCUD, BODY);
  }

  deleteDepartamento(id: any) {
    return this.http.delete(this.rutaCUD + id, this.getOpciones());
  }

}
