import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Observable';

@Injectable()
export class RolService {

  private ruta = 'http://192.168.44.109:4000/api/v1/roles';
  private rutaCUD = 'http://192.168.44.109:4000/api/v1/rol/';

  constructor(private http: Http) { }

  getOpciones(): RequestOptions {
    const HEADERS = new Headers({ name: 'application/json' });
    const REQOPTIONS = new RequestOptions({ headers: HEADERS });

    return REQOPTIONS;
  }

  getRoles() {
    return this.http.get(this.ruta, this.getOpciones());
  }

  getPersonas(idRol) {
    return this.http.get(this.rutaCUD + idRol + '/personas', this.getOpciones());
  }

  createRol(descripcion: string) {
    const BODY = { descripcion: descripcion };
    return this.http.post(this.rutaCUD, BODY);
  }

  updateRol(idRol: number, descripcion: string) {
    const BODY = { idRol: idRol, descripcion: descripcion };
    return this.http.put(this.rutaCUD, BODY);
  }

  deleteRol(idRol: any) {
    return this.http.delete(this.rutaCUD + idRol, this.getOpciones());
  }
}
