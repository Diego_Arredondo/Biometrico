import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class AsistenciaService {

  private ruta = 'http://192.168.44.109:4000/api/v1/asistencias';
  private rutaCUD = 'http://192.168.44.109:4000/api/v1/asistencia/';

  constructor(private http: Http) { }

  getOpciones(): RequestOptions {
    const HEADERS = new Headers({ name: 'application/json' });
    const REQOPTIONS = new RequestOptions({ headers: HEADERS });

    return REQOPTIONS;
  }

  getAsistencias() {
    return this.http.get(this.ruta, this.getOpciones());
  }

  createAsistencia(obj: any) {
    const BODY = obj;
    return this.http.post(this.rutaCUD, BODY);
  }

  updateAsistencia(obj: any) {
    const BODY = obj;
    return this.http.put(this.rutaCUD, BODY);
  }

  deleteAsistencia(idAsistencia: number) {
    return this.http.delete(this.rutaCUD + idAsistencia, this.getOpciones());
  }

}
