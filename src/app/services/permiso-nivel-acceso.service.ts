import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Observable';

@Injectable()
export class PermisoNivelAccesoService {

  private ruta = 'http://192.168.44.109:4000/api/v1/permisoNivelAccesos';
  private rutaCUD = 'http://192.168.44.109:4000/api/v1/permisoNivelAcceso/';

  constructor(private http: Http) { }

  getOpciones(): RequestOptions {
    const HEADERS = new Headers({ name: 'application/json' });
    const REQOPTIONS = new RequestOptions({ headers: HEADERS });

    return REQOPTIONS;
  }

  getPermisoNivelAcceso() {
    return this.http.get(this.ruta, this.getOpciones());
  }

  createPermisoNivelAcceso(obj: any) {
    const BODY = obj;
    return this.http.post(this.rutaCUD, BODY);
  }

  updatePermisoNivelAcceso(obj: any) {
    const BODY = obj;
    return this.http.put(this.rutaCUD, BODY);
  }

  deletePermisoNivelAcceso(id: number) {
    return this.http.delete(this.rutaCUD + id, this.getOpciones());
  }

}
