import { Injectable } from '@angular/core';

@Injectable()
export class UbicacionService {

  constructor() { }

  private Departamentos = [
    {
      guatemala: [
        'Amatitlán',
        'Guatemala',
        'San José Pinula',
        'San Pedro Sacatepéquez',
        'Villa Nueva',
        'Chinautla',
        'Mixco',
        'San Juan Sacatepéquez',
        'San Raymundo',
        'Chuarrancho',
        'Palencia',
        'San Miguel Petapa',
        'Santa Catarina Pinula',
        'Fraijanes',
        'San José del Golfo',
        'San Pedro Ayampuc',
        'Villa Canales'
      ]
    },
    {
      altaVerapaz: [
        'Chahal',
        'Lanquín',
        'San Juan Chamelco',
        'Santa María Cahabón',
        'Tucurú',
        'Chisec',
        'Panzós',
        'San Pedro Carchá',
        'Senahú',
        'Cobán',
        'Raxruhá',
        'Santa Catalina La Tinta',
        'Tactic',
        'Fray Bartolomé de las Casas',
        'San Cristóbal Verapaz',
        'Santa Cruz Verapaz',
        'Tamahú'
      ]
    }
  ];

  getUbicacion() {
    return this.Departamentos;
  }
}
