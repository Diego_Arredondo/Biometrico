import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Observable';

@Injectable()
export class PermisoService {

  private ruta = 'http://192.168.44.109:4000/api/v1/permisos';
  private rutaCUD = 'http://192.168.44.109:4000/api/v1/permiso/';

  constructor(private http: Http) { }

  getOpciones(): RequestOptions {
    const HEADERS = new Headers({ name: 'application/json' });
    const REQOPTIONS = new RequestOptions({ headers: HEADERS });

    return REQOPTIONS;
  }

  getPermisos() {
    return this.http.get(this.ruta, this.getOpciones());
  }

  getPersonas(idPermiso) {
    return this.http.get(this.rutaCUD + idPermiso + '/personas', this.getOpciones());
  }

  createPermiso(descripcion: string, codigo: string) {
    const BODY = { descripcion: descripcion, codigo: codigo };
    return this.http.post(this.rutaCUD, BODY);
  }

  updatePermiso(id: number, descripcion: string, codigo: string) {
    const BODY = { idPermiso: id, descripcion: descripcion, codigo: codigo };
    return this.http.put(this.rutaCUD, BODY);
  }

  deletePermiso(id: number) {
    return this.http.delete(this.rutaCUD + id, this.getOpciones());
  }

}
