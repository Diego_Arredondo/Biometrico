import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class PersonaService {

  private ruta = 'http://192.168.44.109:4000/api/v1/personas';
  private rutaCUD = 'http://192.168.44.109:4000/api/v1/persona/';

  constructor(private http: Http) { }

  getOpciones(): RequestOptions {
    const HEADERS = new Headers({ name: 'application/json' });
    const REQOPTIONS = new RequestOptions({ headers: HEADERS });

    return REQOPTIONS;
  }

  getPersonas() {
    return this.http.get(this.ruta, this.getOpciones());
  }

  getPersona(idPersona: number) {
    return this.http.get(this.rutaCUD + idPersona, this.getOpciones());
  }

  createPersona(obj: any) {
    const BODY = obj;
    return this.http.post(this.rutaCUD, BODY);
  }

  updatePersona(obj: any) {
    const BODY = obj;
    return this.http.put(this.rutaCUD, BODY);
  }

  deletePersona(idPersona: number) {
    return this.http.delete(this.rutaCUD + idPersona, this.getOpciones());
  }
}
