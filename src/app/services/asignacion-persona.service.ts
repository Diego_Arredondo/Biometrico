import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class AsignacionPersonaService {

  private ruta = 'http://192.168.44.109:4000/api/v1/asignacionPersonas';
  private rutaCUD = 'http://192.168.44.109:4000/api/v1/asignacionPersona/';

  constructor(private http: Http) { }

  getOpciones() {
    const HEADERS = new Headers({ name: 'application/json' });
    const REQOPTIONS = new RequestOptions({ headers: HEADERS });

    return REQOPTIONS;
  }

  getAsignacionPersonas() {
    return this.http.get(this.ruta, this.getOpciones());
  }

  createAsigancionPersona(obj: any) {
    const BODY = obj;
    return this.http.post(this.rutaCUD, BODY);
  }

  updateAsignacionPersona(obj: any) {
    const BODY = obj;
    return this.http.put(this.rutaCUD, BODY);
  }

  deleteAsignacionPersona(idAsignacion) {
    return this.http.delete(this.rutaCUD + idAsignacion, this.getOpciones());
  }
}
