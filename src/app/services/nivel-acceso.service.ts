import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Observable';

@Injectable()
export class NivelAccesoService {

  private ruta = 'http://192.168.44.109:4000/api/v1/nivelesAcceso';
  private rutaCUD = 'http://192.168.44.109:4000/api/v1/nivelAcceso/';

  constructor(private http: Http) { }

  getOpciones(): RequestOptions {
    const HEADERS = new Headers({ name: 'application/json' });
    const REQOPTIONS = new RequestOptions({ headers: HEADERS });

    return REQOPTIONS;
  }

  getNivelesAcceso() {
    return this.http.get(this.ruta, this.getOpciones());
  }

  getPersonas(idNivelAcceso) {
    return this.http.get(this.rutaCUD + idNivelAcceso + '/personas', this.getOpciones());
  }

  createNivelAcceso(descripcion: string, codigo: string) {
    const BODY = { descripcion: descripcion, codigo: codigo };
    return this.http.post(this.rutaCUD, BODY);
  }

  updateNivelAcceso(id: number, descripcion: string, codigo: string) {
    const BODY = { idNivelAcceso: id, descripcion: descripcion, codigo: codigo };
    return this.http.put(this.rutaCUD, BODY);
  }

  deleteNivelAcceso(id: number) {
    return this.http.delete(this.rutaCUD + id, this.getOpciones());
  }
}
