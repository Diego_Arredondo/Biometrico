import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class UsuarioService {

  private ruta = 'http://192.168.44.109:4000/api/v1/usuarios';
  private rutaCUD = 'http://192.168.44.109:4000/api/v1/usuario/';

  constructor(private http: Http) { }

  getOpciones(): RequestOptions {
    const HEADERS = new Headers({ name: 'application/json' });
    const REQOPTIONS = new RequestOptions({ headers: HEADERS });

    return REQOPTIONS;
  }

  getUsuarios() {
    return this.http.get(this.ruta, this.getOpciones());
  }

  createUsuario(obj: any) {
    const BODY = obj;
    return this.http.post(this.rutaCUD, BODY);
  }

  updateUsuario(obj: any) {
    const BODY = obj;
    return this.http.put(this.rutaCUD, BODY);
  }

  deleteUsuario(idUsuario: number) {
    return this.http.delete(this.rutaCUD + idUsuario, this.getOpciones());
  }

}
