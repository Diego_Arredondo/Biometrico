import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DepartamentosComponent } from './components/departamentos/departamentos.component';
import { RolesComponent } from './components/roles/roles.component';
import { PermisosComponent } from './components/permisos/permisos.component';
import { NivelAccesoComponent } from './components/nivel-acceso/nivel-acceso.component';
import { PermisoNivelAccesoComponent } from './components/permiso-nivel-acceso/permiso-nivel-acceso.component';
import { AsistenciasComponent } from './components/asistencias/asistencias.component';
import { LoginComponent } from './components/login/login.component';
import { PersonasComponent } from './components/personas/personas.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { CreatePersonaComponent } from './components/personas/create-persona/create-persona.component';
import { DetailsPersonaComponent } from './components/personas/details-persona/details-persona.component';
import { UpdatePersonaComponent } from './components/personas/update-persona/update-persona.component';
import { AsignacionPersonasComponent } from './components/asignacion-personas/asignacion-personas.component';


const APP_ROUTES: Routes = [
    { path: 'home', component: DashboardComponent },
    { path: 'departamentos', component: DepartamentosComponent },
    { path: 'roles', component: RolesComponent },
    { path: 'permisos', component: PermisosComponent },
    { path: 'nivelacceso', component: NivelAccesoComponent },
    { path: 'permisonivelacceso', component: PermisoNivelAccesoComponent },
    { path: 'asistencia', component: AsistenciasComponent },
    { path: 'personas', component: PersonasComponent },
    { path: 'asignacionpersona', component: AsignacionPersonasComponent },
    { path: 'usuarios', component: UsuariosComponent },
    { path: 'personas/createp', component: CreatePersonaComponent },
    { path: 'personas/:idPersona', component: DetailsPersonaComponent },
    { path: 'personas/editar/:idPersona', component: UpdatePersonaComponent },
    { path: '**', pathMatch: 'full', component: LoginComponent }

];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
