import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NivelAccesoService } from '../../services/nivel-acceso.service';

@Component({
  selector: 'app-nivel-acceso',
  templateUrl: './nivel-acceso.component.html',
  styleUrls: ['./nivel-acceso.component.css']
})
export class NivelAccesoComponent implements OnInit {
  lista = [];
  listaFija = [];
  listaPersonas = [];
  codigo: string;
  descripcion: string;
  idNivelAcceso: number;
  data: string;
  closeResult: string;
  msgError = false;
  showMessage: string;
  private modalRef: NgbModalRef;

  constructor(private nivelAccesoService: NivelAccesoService, private modalService: NgbModal) { }

  ngOnInit() {
    this.getNivelesAcceso();
  }

  burcarNivelAcceso(text: any) {
    this.lista = this.listaFija;
    const list: any = [];
    if (text !== '') {
      for (const elemento of this.lista) {
        if (elemento.descripcion.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
        elemento.codigo.toLowerCase().indexOf(text.toLowerCase()) > -1) {
          list.push(elemento);
          this.lista = list;
          this.msgError = false;
        } else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError = true;
          }
        }
      }
    } else {
      this.getNivelesAcceso();
      this.msgError = false;
    }
  }

  getNivelesAcceso() {
    this.nivelAccesoService.getNivelesAcceso().subscribe(res => {
      this.lista = res.json();
      this.listaFija = res.json();
    });
  }

  getPersonas() {
    this.nivelAccesoService.getPersonas(this.idNivelAcceso).subscribe(res => {
      this.listaPersonas = res.json();
    });
  }

  createNivelAcceso() {
    if (this.descripcion !== '' && this.codigo !== '') {
      this.nivelAccesoService.createNivelAcceso(this.descripcion, this.codigo).subscribe(
        (datos) => {
          this.getNivelesAcceso();
          this.modalRef.close();
        },
        (err) => {
          this.showMessage = err._body;
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos';
    }
  }

  updateNivelAcceso() {
    if (this.descripcion !== '' && this.codigo !== '') {
      this.nivelAccesoService.updateNivelAcceso(this.idNivelAcceso, this.descripcion, this.codigo).subscribe(
        (datos) => {
          this.getNivelesAcceso();
          this.modalRef.close();
        },
        (err) => {
          this.showMessage = err._body;
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos';
    }
  }

  deleteNivelAcceso() {
    this.nivelAccesoService.deleteNivelAcceso(this.idNivelAcceso).subscribe(
      (datos) => {
        this.getNivelesAcceso();
      }
    );
  }
  setData(item: any) {
    this.idNivelAcceso = item.idNivelAcceso;
    this.descripcion = item.descripcion;
    this.codigo = item.codigo;
    this.showMessage = '';
  }

  deleteData(item: any) {
    this.descripcion = '';
    this.codigo = '';
    this.showMessage = '';
  }

  open(content) {
    this.modalRef = this.modalService.open(content);
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
