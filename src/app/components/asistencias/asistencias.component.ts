import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AsistenciaService } from '../../services/asistencia.service';
import { PersonaService } from '../../services/persona.service';
import { Time } from '@angular/common/src/i18n/locale_data_api';

@Component({
  selector: 'app-asistencias',
  templateUrl: './asistencias.component.html',
  styleUrls: ['./asistencias.component.css']
})
export class AsistenciasComponent implements OnInit {

  lista = [];
  listaPersona = [];
  listaFija = [];
  primerNombre: string;
  primerApellido: string;
  idPersona: number;
  idAsistencia: number;
  horaEntrada: Time;
  horaSalida: Time;
  fecha: Date;
  data: string;
  msgError = false;
  closeResult: string;
  showMessage: string;
  private modalRef: NgbModalRef;

  constructor(private asistenciaService: AsistenciaService, private personaService: PersonaService, private modalService: NgbModal) { }

  ngOnInit() {
    this.getAsistencias();
    this.getPersonas();
  }

  burcarAsistencia(text: any) {
    this.lista = this.listaFija;
    const list: any = [];
    if (text !== '') {
      for (const elemento of this.lista) {
        if (elemento.persona.primerNombre.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
        elemento.persona.primerApellido.toLowerCase().indexOf(text.toLowerCase()) > -1) {
          list.push(elemento);
          this.lista = list;
          this.msgError = false;
        } else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError = true;
          }
        }
      }
    } else {
      this.getAsistencias();
      this.msgError = false;
    }
  }

  getAsistencias() {
    this.asistenciaService.getAsistencias().subscribe(res => {
      this.lista = res.json();
      this.listaFija = res.json();
    });
  }


  getPersonas() {
    this.personaService.getPersonas().subscribe(res => {
      this.listaPersona = res.json();
    });
  }


  createAsistencia() {
    if (this.idPersona !== null && this.fecha !== null && this.horaEntrada !== null && this.horaSalida !== null) {
      const asistencia = {
        persona: {
          idPersona: this.listaPersona[this.idPersona].idPersona,
          activo: this.listaPersona[this.idPersona].activo,
          cui: this.listaPersona[this.idPersona].cui,
          primerNombre: this.listaPersona[this.idPersona].primerNombre,
          primerApellido: this.listaPersona[this.idPersona].primerApellido,
          genero: this.listaPersona[this.idPersona].genero,
          estadoCivil: this.listaPersona[this.idPersona].estadoCivil,
          sabeLeer: this.listaPersona[this.idPersona].sabeLeer,
          sabeEscribir: this.listaPersona[this.idPersona].sabeEscribir,
          profesion: this.listaPersona[this.idPersona].profesion,
          fotografia: this.listaPersona[this.idPersona].fotografia,
          huellaDactilar: this.listaPersona[this.idPersona].huellaDactilar
        },
        fecha: this.fecha,
        horaEntrada: this.horaEntrada + ':00',
        horaSalida: this.horaSalida + ':00'
      };

      this.asistenciaService.createAsistencia(asistencia).subscribe(
        (datos) => {
          this.getAsistencias();
          this.modalRef.close();
        },
        (err) => {
          this.showMessage = err._body;
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos';
    }

  }

  updateAsistencia() {
    if (this.idPersona !== null && this.fecha !== null && this.horaEntrada !== null && this.horaSalida !== null) {
      const asistencia = {
        idAsistencia: this.idAsistencia,
        persona: {
          idPersona: this.listaPersona[this.idPersona].idPersona,
          activo: this.listaPersona[this.idPersona].activo,
          cui: this.listaPersona[this.idPersona].cui,
          primerNombre: this.listaPersona[this.idPersona].primerNombre,
          primerApellido: this.listaPersona[this.idPersona].primerApellido,
          genero: this.listaPersona[this.idPersona].genero,
          estadoCivil: this.listaPersona[this.idPersona].estadoCivil,
          sabeLeer: this.listaPersona[this.idPersona].sabeLeer,
          sabeEscribir: this.listaPersona[this.idPersona].sabeEscribir,
          profesion: this.listaPersona[this.idPersona].profesion,
          fotografia: this.listaPersona[this.idPersona].fotografia,
          huellaDactilar: this.listaPersona[this.idPersona].huellaDactilar
        },
        fecha: this.fecha,
        horaEntrada: this.horaEntrada,
        horaSalida: this.horaSalida
      };

      this.asistenciaService.updateAsistencia(asistencia).subscribe(
        (datos) => {
          this.getAsistencias();
          this.modalRef.close();
        },
        (err) => {
          this.showMessage = err._body;
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos';
    }
  }

  deleteAsistencia() {
    this.asistenciaService.deleteAsistencia(this.idAsistencia).subscribe(
      (datos) => {
        this.getAsistencias();
      }
    );
  }

  setData(item: any) {
    this.idAsistencia = item.idAsistencia;
    this.idPersona = this.listaPersona.findIndex(obj => obj.cui === item.persona.cui);
    this.fecha = item.fecha;
    this.horaEntrada = item.horaEntrada;
    this.horaSalida = item.horaSalida;
    this.primerNombre = item.persona.primerNombre;
    this.primerApellido = item.persona.primerApellido;
    this.showMessage = '';
  }

  deleteData(item: any) {
    this.idPersona = null;
    this.fecha = null;
    this.horaEntrada = null;
    this.horaSalida = null;
    this.primerNombre = '';
    this.primerApellido = '';
    this.showMessage = '';
  }

  open(content) {
    this.modalRef = this.modalService.open(content);
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
