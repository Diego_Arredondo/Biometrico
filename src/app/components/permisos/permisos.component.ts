import { Component, OnInit } from '@angular/core';
import { PermisoService } from '../../services/permiso.service';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-permisos',
  templateUrl: './permisos.component.html',
  styleUrls: ['./permisos.component.css']
})
export class PermisosComponent implements OnInit {
  lista = [];
  listaFija = [];
  listaPersonas = [];
  codigo: string;
  descripcion: string;
  idPermiso: number;
  data: string;
  closeResult: string;
  msgError = false;
  showMessage: string;
  private modalRef: NgbModalRef;

  constructor(private permisoService: PermisoService, private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getPermisos();
  }

  buscarPermiso(text: any) {
    this.lista = this.listaFija;
    const list: any = [];
    if (text !== '') {
      for (const elemento of this.lista) {
        if (elemento.descripcion.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
        elemento.codigo.toLowerCase().indexOf(text.toLowerCase()) > -1) {
          list.push(elemento);
          this.lista = list;
          this.msgError = false;
        } else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError = true;
          }
        }
      }
    } else {
      this.getPermisos();
      this.msgError = false;
    }
  }

  getPermisos() {
    this.permisoService.getPermisos().subscribe(res => {
      this.lista = res.json();
      this.listaFija = res.json();
    });
  }

  getPersonas() {
    this.permisoService.getPersonas(this.idPermiso).subscribe(res => {
      this.listaPersonas = res.json();
    });
  }

  createPermiso() {
    if (this.descripcion !== '' && this.codigo !== '') {
      this.permisoService.createPermiso(this.descripcion, this.codigo).subscribe(
        (data) => {
          this.getPermisos();
          this.modalRef.close();
        },
        (err) => {
          this.showMessage = err._body;
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos';
    }
  }

  updatePermiso() {
    if (this.descripcion !== '' && this.codigo !== '') {
      this.permisoService.updatePermiso(this.idPermiso, this.descripcion, this.codigo).subscribe(
        (data) => {
          this.getPermisos();
          this.modalRef.close();
        },
        (err) => {
          this.showMessage = err._body;
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos';
    }
  }

  deletePermiso() {
    this.permisoService.deletePermiso(this.idPermiso).subscribe(
      (data) => {
        this.getPermisos();
      }
    );
  }

  setData(item: any) {
    this.idPermiso = item.idPermiso;
    this.descripcion = item.descripcion;
    this.codigo = item.codigo;
    this.showMessage = '';
  }

  deleteData(item: any) {
    this.descripcion = '';
    this.codigo = '';
    this.showMessage = '';
  }

  open(content) {
    this.modalRef = this.modalService.open(content);
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
