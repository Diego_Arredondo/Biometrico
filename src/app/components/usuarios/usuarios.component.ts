import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { PersonaService } from '../../services/persona.service';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { dashCaseToCamelCase } from '@angular/compiler/src/util';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  lista = [];
  listaPersona = [];
  listaFija = [];
  primerNombre: string;
  primerApellido: string;
  idPersona: number;
  idUsuario: number;
  usuario: string;
  password: string;
  remember_token: string;
  data: string;
  msgError = false;
  closeResult: string;
  showMessage: string;
  private modalRef: NgbModalRef;

  constructor(private usuarioService: UsuarioService, private personaService: PersonaService, private modalService: NgbModal) { }

  ngOnInit() {
    this.getUsuarios();
    this.getPersonas();
  }

  buscarUsuario(text: any) {
    this.lista = this.listaFija;
    const list: any = [];
    if (text !== '') {
      for (const elemento of this.lista) {
        if (elemento.persona.primerNombre.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
        elemento.persona.primerApellido.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
        elemento.usuario.toLowerCase().indexOf(text.toLowerCase()) > -1) {
          list.push(elemento);
          this.lista = list;
          this.msgError = false;
        } else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError = true;
          }
        }
      }
    } else {
      this.getUsuarios();
      this.msgError = false;
    }
  }

  getUsuarios() {
    this.usuarioService.getUsuarios().subscribe(res => {
      this.lista = res.json();
      this.listaFija = res.json();
    });
  }


  getPersonas() {
    this.personaService.getPersonas().subscribe(res => {
      this.listaPersona = res.json();
    });
  }

  createUsuario() {
    if (this.idPersona !== null && this.usuario !== '' && this.password !== '' && this.remember_token !== '') {
      const usuario = {
        usuario: this.usuario,
        password: this.password,
        remember_token: this.remember_token,
        persona: {
          idPersona: this.listaPersona[this.idPersona].idPersona,
          activo: this.listaPersona[this.idPersona].activo,
          cui: this.listaPersona[this.idPersona].cui,
          primerNombre: this.listaPersona[this.idPersona].primerNombre,
          primerApellido: this.listaPersona[this.idPersona].primerApellido,
          genero: this.listaPersona[this.idPersona].genero,
          estadoCivil: this.listaPersona[this.idPersona].estadoCivil,
          sabeLeer: this.listaPersona[this.idPersona].sabeLeer,
          sabeEscribir: this.listaPersona[this.idPersona].sabeEscribir,
          profesion: this.listaPersona[this.idPersona].profesion,
          fotografia: this.listaPersona[this.idPersona].fotografia,
          huellaDactilar: this.listaPersona[this.idPersona].huellaDactilar
        },
      };

      this.usuarioService.createUsuario(usuario).subscribe(
        (datos) => {
          this.getUsuarios();
          this.modalRef.close();
        },
        (err) => {
          this.showMessage = err._body;
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos';
    }
  }

  updateUsuario() {
    if (this.idPersona !== null && this.usuario !== '' && this.password !== '' && this.remember_token !== '') {
      const usuario = {
        idUsuario: this.idUsuario,
        usuario: this.usuario,
        password: this.password,
        remember_token: this.remember_token,
        persona: {
          idPersona: this.listaPersona[this.idPersona].idPersona,
          activo: this.listaPersona[this.idPersona].activo,
          cui: this.listaPersona[this.idPersona].cui,
          primerNombre: this.listaPersona[this.idPersona].primerNombre,
          primerApellido: this.listaPersona[this.idPersona].primerApellido,
          genero: this.listaPersona[this.idPersona].genero,
          estadoCivil: this.listaPersona[this.idPersona].estadoCivil,
          sabeLeer: this.listaPersona[this.idPersona].sabeLeer,
          sabeEscribir: this.listaPersona[this.idPersona].sabeEscribir,
          profesion: this.listaPersona[this.idPersona].profesion,
          fotografia: this.listaPersona[this.idPersona].fotografia,
          huellaDactilar: this.listaPersona[this.idPersona].huellaDactilar
        },
      };
      this.usuarioService.updateUsuario(usuario).subscribe(
        (datos) => {
          this.getUsuarios();
          this.modalRef.close();
        },
        (err) => {
          this.showMessage = err._body;
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos';
    }
  }

  deleteUsuario() {
    this.usuarioService.deleteUsuario(this.idUsuario).subscribe(
      (data) => {
        this.getUsuarios();
      }
    );
  }

  setData(item: any) {
    this.idUsuario = item.idUsuario;
    this.idPersona = this.listaPersona.findIndex(obj => obj.cui === item.persona.cui);
    this.usuario = item.usuario;
    this.password = item.password;
    this.remember_token = item.remember_token;
    this.primerNombre = item.persona.primerNombre;
    this.primerApellido = item.persona.primerApellido;
    this.showMessage = '';
  }

  deleteData(item: any) {
    this.idPersona = null;
    this.usuario = '';
    this.password = '';
    this.remember_token = '';
    this.primerNombre = '';
    this.primerApellido = '';
    this.showMessage = '';
  }


  open(content) {
    this.modalRef = this.modalService.open(content);
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
