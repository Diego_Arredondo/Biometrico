import { Component, OnInit } from '@angular/core';
import { DepartamentoService } from '../../services/departamento.service';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-departamentos',
  templateUrl: './departamentos.component.html',
  styleUrls: ['./departamentos.component.css']
})
export class DepartamentosComponent implements OnInit {
  lista = [];
  listaFija = [];
  listaPersonas = [];
  nombreDepartamento: string;
  idDepartamento: number;
  closeResult: string;
  data: string;
  msgError = false;
  showMessage: string;
  private modalRef: NgbModalRef;

  constructor(private departamentoService: DepartamentoService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getDepartamentos();
  }

  buscarDepartamento(text: any) {
    this.lista = this.listaFija;
    const list: any = [];
    if (text !== '') {
      for (const elemento of this.lista) {
        if (elemento.nombreDepartamento.toLowerCase().indexOf(text.toLowerCase()) > -1) {
          list.push(elemento);
          this.lista = list;
          this.msgError = false;
        } else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError = true;
          }
        }
      }
    } else {
      this.getDepartamentos();
      this.msgError = false;
    }
  }

  getDepartamentos() {
    this.departamentoService.getDepartamentos().subscribe(res => {
      this.lista = res.json();
      this.listaFija = res.json();
    });
  }

  getPersonas() {
    this.departamentoService.getPersonas(this.idDepartamento).subscribe(res => {
      this.listaPersonas = res.json();
    });
  }

  createDepartamento() {
    if (this.nombreDepartamento !== '') {
      this.departamentoService.createDepartamento(this.nombreDepartamento).subscribe(
        (data) => {
          this.getDepartamentos();
          this.modalRef.close();
        },
        (err) => {
          this.showMessage = err._body;
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos';
    }
  }

  updateDepartamento() {
    if (this.nombreDepartamento !== '') {
      this.departamentoService.updateDepartamento(this.idDepartamento, this.nombreDepartamento).subscribe(
        (data) => {
          this.getDepartamentos();
          this.modalRef.close();
        },
        (err) => {
          this.showMessage = err._body;
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos';
    }
  }

  deleteDepartamento() {
    this.departamentoService.deleteDepartamento(this.idDepartamento).subscribe(
      (data) => {
        this.getDepartamentos();
      }
    );
  }

  setData(item: any) {
    this.nombreDepartamento = item.nombreDepartamento;
    this.idDepartamento = item.idDepartamento;
    this.showMessage = '';
  }

  deleteData(item: any) {
    this.nombreDepartamento = '';
    this.showMessage = '';
  }

  open(content) {
    this.modalRef = this.modalService.open(content);
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}


