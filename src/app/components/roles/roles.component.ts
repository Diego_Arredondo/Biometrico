import { Component, OnInit } from '@angular/core';
import { RolService } from '../../services/rol.service';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  lista = [];
  listaFija = [];
  listaPersonas = [];
  descripcion: string;
  idRol: number;
  closeResult: string;
  msgError = false;
  data: string;
  showMessage: string;
  private modalRef: NgbModalRef;

  constructor(private rolService: RolService, private modalService: NgbModal) { }

  ngOnInit() {
    this.getRoles();
  }

  buscarRol(text: any) {
    this.lista = this.listaFija;
    const list: any = [];
    if (text !== '') {
      for (const elemento of this.lista) {
        if (elemento.descripcion.toLowerCase().indexOf(text.toLowerCase()) > -1) {
          list.push(elemento);
          this.lista = list;
          this.msgError = false;
        } else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError = true;
          }
        }
      }
    } else {
      this.getRoles();
      this.msgError = false;
    }
  }

  getRoles() {
    this.rolService.getRoles().subscribe(res => {
      this.lista = res.json();
      this.listaFija = res.json();
    });
  }

  getPersonas() {
    this.rolService.getPersonas(this.idRol).subscribe(res => {
      this.listaPersonas = res.json();
    });
  }

  createRol() {
    if (this.descripcion !== '') {
      this.rolService.createRol(this.descripcion).subscribe(
        (data) => {
          this.getRoles();
          this.modalRef.close();
        },
        (err) => {
          this.showMessage = err._body;
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos';
    }
  }

  updateRol() {
    if (this.descripcion !== '') {
      this.rolService.updateRol(this.idRol, this.descripcion).subscribe(
        (data) => {
          this.getRoles();
          this.modalRef.close();
        },
        (err) => {
          this.showMessage = err._body;
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos';
    }
  }

  deleteRol() {
    this.rolService.deleteRol(this.idRol).subscribe(
      (data) => {
        this.getRoles();
      }
    );
  }

  setData(item: any) {
    this.descripcion = item.descripcion;
    this.idRol = item.idRol;
    this.showMessage = '';
  }

  deleteData(item: any) {
    this.descripcion = '';
    this.showMessage = '';
  }

  open(content) {
    this.modalRef = this.modalService.open(content);
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


}
