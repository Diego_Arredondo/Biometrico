import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonaService } from '../../../services/persona.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-update-persona',
  templateUrl: './update-persona.component.html',
  styleUrls: ['./update-persona.component.css']
})
export class UpdatePersonaComponent implements OnInit {

  idPersona: number;
  activo: number;
  cui: number;
  primerNombre: string;
  segundoNombre: string;
  tercerNombre: string;
  primerApellido: string;
  segundoApellido: string;
  tercerApellido: string;
  genero: string;
  nacionalidadMunicipio: string;
  nacionalidadDepartamento: string;
  nacionalidadPais: string;
  fechaNacimiento: Date;
  fechaEmisionDocumento: Date;
  fechaVencimientoDocumento: Date;
  estadoCivil: string;
  vecindadMunicipio: string;
  vecindadDepartamento: string;
  nacionalidad: string;
  sabeLeer: string;
  sabeEscribir: string;
  limitacionesFisicas: string;
  libro: string;
  folio: string;
  partida: string;
  residenciaDireccion: string;
  residenciaMunicipio: string;
  residenciaDepartamento: string;
  codigoPostal: string;
  residenciaPais: string;
  profesion: string;
  cedulaNumero: string;
  cedulaMunicipio: string;
  cedulaDepartamento: string;
  oficialActivo: string;
  fotografia: 1;
  numeroSerieDocumento: string;
  correoPersonal: string;
  correoInstitucional: string;
  numeroTelefonico: string;
  numeroTelefonicoMovil: string;
  mrz: string;
  persona = [];
  closeResult: string;
  showMessage: string;

  constructor(private activatedRoute: ActivatedRoute,
    private personaService: PersonaService,
    private router: Router,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(id => {
      this.idPersona = id['idPersona'];
    });
    this.getPersona();
  }

  getPersona() {
    this.personaService.getPersona(this.idPersona).subscribe(
      res => {
        this.persona = res.json();
        this.setData(this.persona);
      }
    );
  }

  updatePersona(modal) {
    if (this.activo !== undefined && this.cui !== undefined && this.primerNombre !== '' && this.primerApellido !== ''
      && this.sabeEscribir !== undefined && this.sabeLeer !== undefined && this.profesion !== '' && this.estadoCivil !== undefined
      && this.genero !== undefined && this.oficialActivo !== undefined) {
      const persona = {
        idPersona: this.idPersona,
        activo: this.activo,
        cui: this.cui,
        primerNombre: this.primerNombre,
        segundoNombre: this.segundoNombre,
        tercerNombre: this.tercerNombre,
        primerApellido: this.primerApellido,
        segundoApellido: this.segundoApellido,
        tercerApellido: this.tercerApellido,
        genero: this.genero,
        nacionalidadMunicipio: this.nacionalidadMunicipio,
        nacionalidadDepartamento: this.nacionalidadDepartamento,
        nacionalidadPais: this.nacionalidadPais,
        fechaNacimiento: this.fechaNacimiento,
        fechaEmisionDocumento: this.fechaEmisionDocumento,
        fechaVencimientoDocumento: this.fechaVencimientoDocumento,
        estadoCivil: this.estadoCivil,
        vecindadMunicipio: this.vecindadMunicipio,
        vecindadDepartamento: this.vecindadDepartamento,
        nacionalidad: this.nacionalidad,
        sabeLeer: this.sabeLeer,
        sabeEscribir: this.sabeEscribir,
        limitacionesFisicas: this.limitacionesFisicas,
        libro: this.libro,
        folio: this.folio,
        partida: this.partida,
        residenciaDireccion: this.residenciaDireccion,
        residenciaMunicipio: this.residenciaMunicipio,
        residenciaDepartamento: this.residenciaDepartamento,
        codigoPostal: this.codigoPostal,
        residenciaPais: this.residenciaPais,
        profesion: this.profesion,
        cedulaNumero: this.cedulaNumero,
        cedulaMunicipio: this.cedulaMunicipio,
        cedulaDepartamento: this.cedulaDepartamento,
        oficialActivo: this.oficialActivo,
        fotografia: 1,
        numeroSerieDocumento: this.numeroSerieDocumento,
        correoPersonal: this.correoPersonal,
        correoInstitucional: this.correoInstitucional,
        numeroTelefonico: this.numeroTelefonico,
        numeroTelefonicoMovil: this.numeroTelefonicoMovil,
        mrz: this.mrz

      };
      this.personaService.updatePersona(persona).subscribe(
        (data) => {
          this.router.navigate(['/personas']);
        },
        (err) => {
          this.showMessage = err._body;
          this.open(modal);
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos necesarios';
      this.open(modal);
    }
  }

  setData(item: any) {
    this.idPersona = item.idPersona;
    this.activo = item.activo;
    this.cui = item.cui;
    this.primerNombre = item.primerNombre;
    this.segundoNombre = item.segundoNombre;
    this.tercerNombre = item.tercerNombre;
    this.primerApellido = item.primerApellido;
    this.segundoApellido = item.segundoApellido;
    this.tercerApellido = item.tercerApellido;
    this.genero = item.genero;
    this.nacionalidadMunicipio = item.nacionalidadMunicipio;
    this.nacionalidadDepartamento = item.nacionalidadDepartamento;
    this.nacionalidadPais = item.nacionalidadPais;
    this.fechaNacimiento = item.fechaNacimiento;
    this.fechaEmisionDocumento = item.fechaEmisionDocumento;
    this.fechaVencimientoDocumento = item.fechaVencimientoDocumento;
    this.estadoCivil = item.estadoCivil;
    this.vecindadMunicipio = item.vecindadMunicipio;
    this.vecindadDepartamento = item.vecindadDepartamento;
    this.nacionalidad = item.nacionalidad;
    this.sabeLeer = item.sabeLeer;
    this.sabeEscribir = item.sabeEscribir;
    this.limitacionesFisicas = item.limitacionesFisicas;
    this.libro = item.libro;
    this.folio = item.folio;
    this.partida = item.partida;
    this.residenciaDireccion = item.residenciaDireccion;
    this.residenciaMunicipio = item.residenciaMunicipio;
    this.residenciaDepartamento = item.residenciaDepartamento;
    this.codigoPostal = item.codigoPostal;
    this.residenciaPais = item.residenciaPais;
    this.profesion = item.profesion;
    this.cedulaNumero = item.cedulaNumero;
    this.cedulaMunicipio = item.cedulaMunicipio;
    this.cedulaDepartamento = item.cedulaDepartamento;
    this.oficialActivo = item.oficialActivo;
    this.fotografia = item.fotografia;
    this.numeroSerieDocumento = item.numeroSerieDocumento;
    this.correoPersonal = item.correoPersonal;
    this.correoInstitucional = item.correoInstitucional;
    this.numeroTelefonico = item.numeroTelefonico;
    this.numeroTelefonicoMovil = item.numeroTelefonicoMovil;
    this.mrz = item.mrz;
  }

  personas() {
    this.router.navigate(['/personas']);
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
