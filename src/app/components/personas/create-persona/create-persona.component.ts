import { Component, OnInit } from '@angular/core';
import { PersonaService } from '../../../services/persona.service';
import { Router } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UbicacionService } from '../../../services/ubicacion.service';


@Component({
  selector: 'app-create-persona',
  templateUrl: './create-persona.component.html',
  styleUrls: ['./create-persona.component.css']
})
export class CreatePersonaComponent implements OnInit {

  listaUbicacion = [];
  idPersona: number;
  activo: number;
  cui: number;
  primerNombre: string;
  segundoNombre: string;
  tercerNombre: string;
  primerApellido: string;
  segundoApellido: string;
  tercerApellido: string;
  genero: string;
  nacionalidadMunicipio: string;
  nacionalidadDepartamento: string;
  nacionalidadPais: string;
  fechaNacimiento: Date;
  fechaEmisionDocumento: Date;
  fechaVencimientoDocumento: Date;
  estadoCivil: string;
  vecindadMunicipio: string;
  vecindadDepartamento: string;
  nacionalidad: string;
  sabeLeer: string;
  sabeEscribir: string;
  limitacionesFisicas: string;
  libro: string;
  folio: string;
  partida: string;
  residenciaDireccion: string;
  residenciaMunicipio: string;
  residenciaDepartamento: string;
  codigoPostal: string;
  residenciaPais: string;
  profesion: string;
  cedulaNumero: string;
  cedulaMunicipio: string;
  cedulaDepartamento: string;
  oficialActivo: string;
  fotografia: string;
  numeroSerieDocumento: string;
  correoPersonal: string;
  correoInstitucional: string;
  numeroTelefonico: string;
  numeroTelefonicoMovil: string;
  mrz: string;
  closeResult: string;
  showMessage: string;
  list: [1, 2, 3];

  constructor(private personaService: PersonaService, private router: Router, private modalService: NgbModal,
              private ubicacionService: UbicacionService) { }

  ngOnInit() {
    this.activo = 1;
    this.getUbicaciones();
  }

  createPersona(modal) {
    if (this.activo !== undefined && this.cui !== undefined && this.primerNombre !== undefined && this.primerApellido !== undefined
      && this.sabeEscribir !== undefined && this.sabeLeer !== undefined && this.profesion !== undefined && this.estadoCivil !== undefined
      && this.genero !== undefined && this.oficialActivo !== undefined) {
      const persona = {
        activo: this.activo,
        cui: this.cui,
        primerNombre: this.primerNombre,
        segundoNombre: this.segundoNombre,
        tercerNombre: this.tercerNombre,
        primerApellido: this.primerApellido,
        segundoApellido: this.segundoApellido,
        tercerApellido: this.tercerApellido,
        genero: this.genero,
        nacionalidadMunicipio: this.nacionalidadMunicipio,
        nacionalidadDepartamento: this.nacionalidadDepartamento,
        nacionalidadPais: this.nacionalidadPais,
        fechaNacimiento: this.fechaNacimiento,
        fechaEmisionDocumento: this.fechaEmisionDocumento,
        fechaVencimientoDocumento: this.fechaVencimientoDocumento,
        estadoCivil: this.estadoCivil,
        vecindadMunicipio: this.vecindadMunicipio,
        vecindadDepartamento: this.vecindadDepartamento,
        nacionalidad: this.nacionalidad,
        sabeLeer: this.sabeLeer,
        sabeEscribir: this.sabeEscribir,
        limitacionesFisicas: this.limitacionesFisicas,
        libro: this.libro,
        folio: this.folio,
        partida: this.partida,
        residenciaDireccion: this.residenciaDireccion,
        residenciaMunicipio: this.residenciaMunicipio,
        residenciaDepartamento: this.residenciaDepartamento,
        codigoPostal: this.codigoPostal,
        residenciaPais: this.residenciaPais,
        profesion: this.profesion,
        cedulaNumero: this.cedulaNumero,
        cedulaMunicipio: this.cedulaMunicipio,
        cedulaDepartamento: this.cedulaDepartamento,
        oficialActivo: this.oficialActivo,
        fotografia: this.fotografia,
        numeroSerieDocumento: this.numeroSerieDocumento,
        correoPersonal: this.correoPersonal,
        correoInstitucional: this.correoInstitucional,
        numeroTelefonico: this.numeroTelefonico,
        numeroTelefonicoMovil: this.numeroTelefonicoMovil,
        mrz: this.mrz

      };
      this.personaService.createPersona(persona).subscribe(
        (data) => {
          this.router.navigate(['/personas']);
        },
        (err) => {
          this.showMessage = err._body;
          this.open(modal);
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos necesarios';
      this.open(modal);
    }
  }

  personas() {
    this.router.navigate(['/personas']);
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getUbicaciones() {
    this.listaUbicacion = this.ubicacionService.getUbicacion();
    console.log(this.listaUbicacion);
  }
}
