import { Component, OnInit } from '@angular/core';
import { PersonaService } from '../../services/persona.service';
import { element } from 'protractor';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';


@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.css']
})
export class PersonasComponent implements OnInit {
  lista = [];
  listaFija = [];
  data: string;
  closeResult: string;
  msgError = false;
  idPersona: number;
  primerNombre: string;
  primerApellido: string;
  cui: number;

  constructor(private personaService: PersonaService, private modalService: NgbModal, private router: Router) { }

  ngOnInit() {
    this.getPersonas();
  }

  burcarPersona(text: string) {
    this.lista = this.listaFija;
    const list: any = [];
    if (text !== '') {
      for (const elemento of this.lista) {
        if (elemento.primerNombre.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
        elemento.primerApellido.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
        elemento.cui.toString().indexOf(text.toLowerCase()) > -1) {
          list.push(elemento);
          this.lista = list;
          this.msgError = false;
        } else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError = true;
          }
        }
      }
    } else {
      this.getPersonas();
    }
  }

  getPersonas() {
    this.personaService.getPersonas().subscribe(
      res => {
        this.lista = res.json();
        this.listaFija = res.json();
      }
    );
  }

  deletePersona() {
    this.personaService.deletePersona(this.idPersona).subscribe(
      (data) => {
        this.getPersonas();
      }
    );
  }

  setData(item: any) {
    this.idPersona = item.idPersona;
    this.primerNombre = item.primerNombre;
    this.primerApellido = item.primerApellido;
    this.cui = item.cui;
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  detallePersonas(item: any) {
    this.router.navigate(['/personas', item.idPersona]);
  }

  editarPersona(item: any) {
    this.router.navigate(['/personas/editar', item.idPersona]);
  }
  crearPersonas() {
    this.router.navigate(['/personas/createp']);
  }

}
