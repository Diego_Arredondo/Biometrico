import { Component, OnInit } from '@angular/core';
import { PersonaService } from '../../../services/persona.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-details-persona',
  templateUrl: './details-persona.component.html',
  styleUrls: ['./details-persona.component.css']
})
export class DetailsPersonaComponent implements OnInit {

  idPersona: number;
  persona = [];

  constructor(private activatedRoute: ActivatedRoute, private personaService: PersonaService, private router: Router) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(id => {
      this.idPersona = id['idPersona'];
    });
    this.getPersona();
  }

  getPersona() {
    this.personaService.getPersona(this.idPersona).subscribe(
      res => {
        this.persona = res.json();
      }
    );
  }

  personas() {
    this.router.navigate(['/personas']);
  }
}
