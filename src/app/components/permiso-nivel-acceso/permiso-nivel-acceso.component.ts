import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PermisoNivelAccesoService } from '../../services/permiso-nivel-acceso.service';
import { NivelAccesoService } from '../../services/nivel-acceso.service';
import { PermisoService } from '../../services/permiso.service';

@Component({
  selector: 'app-permiso-nivel-acceso',
  templateUrl: './permiso-nivel-acceso.component.html',
  styleUrls: ['./permiso-nivel-acceso.component.css']
})
export class PermisoNivelAccesoComponent implements OnInit {
  lista = [];
  listaNivelAcceso = [];
  listaPermiso = [];
  listaFija = [];
  codigoN: string;
  codigoP: string;
  descripcionN: string;
  descripcionP: string;
  idPermiso: number;
  idNivelAcceso: number;
  idPermisoNivelAcceso: number;
  data: string;
  closeResult: string;
  msgError = false;
  showMessage: string;
  private modalRef: NgbModalRef;

  constructor(private permisoNivelAccesoService: PermisoNivelAccesoService,
    private nivelAccesoService: NivelAccesoService,
    private permisoService: PermisoService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getPermisoNivelesAcceso();
    this.getNivelesAcceso();
    this.getPermisos();
  }

  buscarPermisoNivelAcceso(text: any) {
    this.lista = this.listaFija;
    const list: any = [];
    if (text !== '') {
      for (const elemento of this.lista) {
        if (elemento.nivelAcceso.descripcion.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
          elemento.permiso.descripcion.toLowerCase().indexOf(text.toLowerCase()) > -1) {
          list.push(elemento);
          this.lista = list;
          this.msgError = false;
        } else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError = true;
          }
        }
      }
    } else {
      this.getPermisoNivelesAcceso();
      this.msgError = false;
    }
  }

  getPermisoNivelesAcceso() {
    this.permisoNivelAccesoService.getPermisoNivelAcceso().subscribe(res => {
      this.lista = res.json();
      this.listaFija = res.json();
    });
  }

  getNivelesAcceso() {
    this.nivelAccesoService.getNivelesAcceso().subscribe(res => {
      this.listaNivelAcceso = res.json();
    });
  }

  getPermisos() {
    this.permisoService.getPermisos().subscribe(res => {
      this.listaPermiso = res.json();
    });
  }


  createPermisoNivelAcceso() {
    if (this.idNivelAcceso !== null && this.idPermiso !== null) {
      const permisoNivelAcceso = {
        nivelAcceso: {
          idNivelAcceso: this.listaNivelAcceso[this.idNivelAcceso].idNivelAcceso,
          descripcion: this.listaNivelAcceso[this.idNivelAcceso].descripcion,
          codigo: this.listaNivelAcceso[this.idNivelAcceso].codigo
        },
        permiso: {
          idPermiso: this.listaPermiso[this.idPermiso].idPermiso,
          descripcion: this.listaPermiso[this.idPermiso].descripcion,
          codigo: this.listaPermiso[this.idPermiso].codigo
        }
      };
      this.permisoNivelAccesoService.createPermisoNivelAcceso(permisoNivelAcceso).subscribe(
        (datos) => {
          this.getPermisoNivelesAcceso();
          this.modalRef.close();
        },
        (err) => {
          this.showMessage = err._body;
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos';
    }
  }

  updatePermisoNivelAcceso() {
    if (this.idNivelAcceso !== null && this.idPermiso !== null) {
      const permisoNivelAcceso = {
        idPermisoNivelAcceso: this.idPermisoNivelAcceso,
        nivelAcceso: {
          idNivelAcceso: this.listaNivelAcceso[this.idNivelAcceso].idNivelAcceso,
          descripcion: this.listaNivelAcceso[this.idNivelAcceso].descripcion,
          codigo: this.listaNivelAcceso[this.idNivelAcceso].codigo
        },
        permiso: {
          idPermiso: this.listaPermiso[this.idPermiso].idPermiso,
          descripcion: this.listaPermiso[this.idPermiso].descripcion,
          codigo: this.listaPermiso[this.idPermiso].codigo
        }
      };
      this.permisoNivelAccesoService.updatePermisoNivelAcceso(permisoNivelAcceso).subscribe(
        (datos) => {
          this.getPermisoNivelesAcceso();
          this.modalRef.close();
        },
        (err) => {
          this.showMessage = err._body;
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos';
    }
  }

  deletePermisoNivelAcceso() {
    this.permisoNivelAccesoService.deletePermisoNivelAcceso(this.idPermisoNivelAcceso).subscribe(
      (datos) => {
        this.getPermisoNivelesAcceso();
      }
    );
  }

  setData(item: any) {
    this.showMessage = '';
    this.idPermisoNivelAcceso = item.idPermisoNivelAcceso;
    this.idNivelAcceso = this.listaNivelAcceso.findIndex(obj => obj.descripcion === item.nivelAcceso.descripcion);
    this.idPermiso = this.listaPermiso.findIndex(obj => obj.descripcion === item.permiso.descripcion);
    this.descripcionN = item.nivelAcceso.descripcion;
    this.codigoN = item.nivelAcceso.codigo;
    this.descripcionP = item.permiso.descripcion;
    this.codigoP = item.permiso.codigo;
  }

  deleteData(item: any) {
    this.showMessage = '';
    this.idNivelAcceso = null;
    this.idPermiso = null;
    this.descripcionN = '';
    this.codigoN = '';
    this.descripcionP = '';
    this.codigoP = '';
  }


  open(content) {
    this.modalRef = this.modalService.open(content);
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


}
