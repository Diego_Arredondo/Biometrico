import { Component, OnInit } from '@angular/core';
import { AsignacionPersonaService } from '../../services/asignacion-persona.service';
import { PersonaService } from '../../services/persona.service';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RolService } from '../../services/rol.service';
import { NivelAccesoService } from '../../services/nivel-acceso.service';
import { DepartamentoService } from '../../services/departamento.service';
import { element } from 'protractor';

@Component({
  selector: 'app-asignacion-personas',
  templateUrl: './asignacion-personas.component.html',
  styleUrls: ['./asignacion-personas.component.css']
})
export class AsignacionPersonasComponent implements OnInit {
  // lista -->recibe los registros de asifnacion persona
  lista = [];
  // lista persona --> recibe los datos de personas
  listaPersona = [];
  listaNivelAcceso = [];
  listaRol = [];
  listaDepartamento = [];
  // listaFija --> solo se usa en el buscar y va a contener lo mismo que la lista
  listaFija = [];
  primerNombre: string;
  primerApellido: string;
  nombreDepartamento: string;
  descripcionRol: string;
  descripcionNivelAcceso: string;
  idPersona: number;
  idNivelAcceso: number;
  idDepartamento: number;
  idRol: number;
  idAsignacion: number;
  data: string;
  msgError = false;
  closeResult: string;
  showMessage: string;
  private modalRef: NgbModalRef;


  constructor(private asignacionPersonaService: AsignacionPersonaService,
    private personaService: PersonaService,
    private rolService: RolService,
    private nivelAccesoService: NivelAccesoService,
    private departamentoService: DepartamentoService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.getAsignacionPersona();
    this.getPersonas();
    this.getDepartamentos();
    this.getNivelAccesos();
    this.getRoles();
  }

  buscarAsignacionPersona(text: any) {
    this.lista = this.listaFija;
    const list: any = [];
    if (text !== '') {
      for (const elemento of this.lista) {
        if (elemento.persona.primerNombre.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
          elemento.persona.primerApellido.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
          elemento.departamento.nombreDepartamento.toLowerCase().indexOf(text.toLowerCase()) > -1) {
          list.push(elemento);
          this.lista = list;
          this.msgError = false;
        } else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError = true;
          }
        }
      }
    } else {
      this.getAsignacionPersona();
      this.msgError = false;
    }
  }

  getAsignacionPersona() {
    this.asignacionPersonaService.getAsignacionPersonas().subscribe(res => {
      this.lista = res.json();
      this.listaFija = res.json();
    }
    );
  }

  getPersonas() {
    this.personaService.getPersonas().subscribe(res => {
      this.listaPersona = res.json();
    }
    );
  }

  getDepartamentos() {
    this.departamentoService.getDepartamentos().subscribe(res => {
      this.listaDepartamento = res.json();
    }
    );
  }

  getRoles() {
    this.rolService.getRoles().subscribe(res => {
      this.listaRol = res.json();
    }
    );
  }

  getNivelAccesos() {
    this.nivelAccesoService.getNivelesAcceso().subscribe(res => {
      this.listaNivelAcceso = res.json();
    }
    );
  }

  createAsignacionPersona() {
    if (this.idPersona !== null && this.idNivelAcceso !== null && this.idDepartamento !== null &&
      this.idRol !== null) {
      const asignacionPersona = {
        persona: {
          idPersona: this.listaPersona[this.idPersona].idPersona,
          activo: this.listaPersona[this.idPersona].activo,
          cui: this.listaPersona[this.idPersona].cui,
          primerNombre: this.listaPersona[this.idPersona].primerNombre,
          primerApellido: this.listaPersona[this.idPersona].primerApellido,
          genero: this.listaPersona[this.idPersona].genero,
          estadoCivil: this.listaPersona[this.idPersona].estadoCivil,
          sabeLeer: this.listaPersona[this.idPersona].sabeLeer,
          sabeEscribir: this.listaPersona[this.idPersona].sabeEscribir,
          profesion: this.listaPersona[this.idPersona].profesion,
          fotografia: this.listaPersona[this.idPersona].fotografia,
          huellaDactilar: this.listaPersona[this.idPersona].huellaDactilar
        },
        departamento: {
          idDepartamento: this.listaDepartamento[this.idDepartamento].idDepartamento,
          nombreDepartamento: this.listaDepartamento[this.idDepartamento].nombreDepartamento
        },
        rol: {
          idRol: this.listaRol[this.idRol].idRol,
          descripcion: this.listaRol[this.idRol].descripcion
        },
        nivelAcceso: {
          idNivelAcceso: this.listaNivelAcceso[this.idNivelAcceso].idNivelAcceso,
          descripcion: this.listaNivelAcceso[this.idNivelAcceso].descripcion,
          codigo: this.listaNivelAcceso[this.idNivelAcceso].codigo
        }
      };
      this.asignacionPersonaService.createAsigancionPersona(asignacionPersona).subscribe(
        (data) => {
          this.getAsignacionPersona();
          this.modalRef.close();
        },
        (err) => {
          this.showMessage = err._body;
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos';
    }
  }

  updateAsignacionPersona() {
    if (this.idPersona !== null && this.idNivelAcceso !== null && this.idDepartamento !== null &&
      this.idRol !== null) {
      const asignacionPersona = {
        idAsignacion: this.idAsignacion,
        persona: {
          idPersona: this.listaPersona[this.idPersona].idPersona,
          activo: this.listaPersona[this.idPersona].activo,
          cui: this.listaPersona[this.idPersona].cui,
          primerNombre: this.listaPersona[this.idPersona].primerNombre,
          primerApellido: this.listaPersona[this.idPersona].primerApellido,
          genero: this.listaPersona[this.idPersona].genero,
          estadoCivil: this.listaPersona[this.idPersona].estadoCivil,
          sabeLeer: this.listaPersona[this.idPersona].sabeLeer,
          sabeEscribir: this.listaPersona[this.idPersona].sabeEscribir,
          profesion: this.listaPersona[this.idPersona].profesion,
          fotografia: this.listaPersona[this.idPersona].fotografia,
          huellaDactilar: this.listaPersona[this.idPersona].huellaDactilar
        },
        departamento: {
          idDepartamento: this.listaDepartamento[this.idDepartamento].idDepartamento,
          nombreDepartamento: this.listaDepartamento[this.idDepartamento].nombreDepartamento
        },
        rol: {
          idRol: this.listaRol[this.idRol].idRol,
          descripcion: this.listaRol[this.idRol].descripcion
        },
        nivelAcceso: {
          idNivelAcceso: this.listaNivelAcceso[this.idNivelAcceso].idNivelAcceso,
          descripcion: this.listaNivelAcceso[this.idNivelAcceso].descripcion,
          codigo: this.listaNivelAcceso[this.idNivelAcceso].codigo
        }
      };
      this.asignacionPersonaService.updateAsignacionPersona(asignacionPersona).subscribe(
        (data) => {
          this.getAsignacionPersona();
          this.modalRef.close();
        },
        (err) => {
          this.showMessage = err._body;
        }
      );
    } else {
      this.showMessage = 'Por favor llene todos los campos';
    }
  }

  deleteAsignacionPersona() {
    this.asignacionPersonaService.deleteAsignacionPersona(this.idAsignacion).subscribe(
      (data) => {
        this.getAsignacionPersona();
      },
      (err) => {
        this.showMessage = err._body;
      }
    );
  }

  setData(item: any) {
    this.idAsignacion = item.idAsignacion;
    this.idDepartamento = this.listaDepartamento.findIndex(obj => obj.nombreDepartamento === item.departamento.nombreDepartamento);
    this.idNivelAcceso = this.listaNivelAcceso.findIndex(obj => obj.descripcion === item.nivelAcceso.descripcion);
    this.idPersona = this.listaPersona.findIndex(obj => obj.cui === item.persona.cui);
    this.idRol = this.listaRol.findIndex(obj => obj.descripcion === item.rol.descripcion);
    this.nombreDepartamento = item.departamento.nombreDepartamento;
    this.descripcionNivelAcceso = item.nivelAcceso.descripcion;
    this.descripcionRol = item.rol.descripcion;
    this.primerNombre = item.persona.primerNombre;
    this.primerApellido = item.persona.primerApellido;
    this.showMessage = '';
  }

  deleteData(item: any) {
    this.idDepartamento = null;
    this.idNivelAcceso = null;
    this.idPersona = null;
    this.idRol = null;
    this.primerNombre = '';
    this.primerApellido = '';
    this.descripcionRol = '';
    this.descripcionNivelAcceso = '';
    this.nombreDepartamento = '';
    this.showMessage = '';
  }

  open(content) {
    this.modalRef = this.modalService.open(content);
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
